# Project Timekiller
Timekiller je aplikacija za deljenje idej med učitelji, animatorji, starši..



# Kako prijavim napako?
Če ste opazili napako, jo lahko nam sporočite na kar tri načine.
- Preko feedback-a (https://d2.si/f/ideja/ap/feedback)
- Preko maila: nujno@d2.si
- (NAJBOLJE) Preko Githuba (kjer ste zdaj)

## Kako prijaviti napako preko GitHUBA?
1. Se registrirajte/prijavite
2. Pojite sem: https://gitlab.com/hypercode/timekiller/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=
3. To je to :) (kliklnite Submit issuse)


# Zapisnik sprememb

### Verzija A1B1C53
- NEW: Feedback sistem

### Verzija A1B1C20
- NEW: Like/dislike sistem
- FIX: Objavlanje idej